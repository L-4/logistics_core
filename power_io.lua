--------------------
-- Power IO module
-- @module power_io
-- @todo: A lot of this functionality could be moved to a generic class, common to all IO.
-- @todo: Delete node cache data for deleted nodes.

-- Make bitwise methods local
local RSHIFT = _G.RSHIFT
local LSHIFT = _G.LSHIFT
local AND = _G.AND
local OR = _G.OR
local NOT = _G.NOT

local log = hutil.make_logger("Logistics Power IO")

local power_io = {}
local power_io_meta = { __index = power_io }
logistics.power_io = power_io

local lp_io_node_cache = logistics.node_cache.new("lp_io")

power_io.node_cache = lp_io_node_cache

local ALL_FACES = 63

--- Offsets to input and output in mask in bits.
local LP_IO_INPUT_OFFSET = 0
local LP_IO_OUTPUT_OFFSET = 6

local LP_IO_MASK = logistics.meta_keys.LP_IO_MASK
local LP_IO_MAX_STORAGE = logistics.meta_keys.LP_IO_MAX_STORAGE
local LP_IO_CURRENT_STORAGE = logistics.meta_keys.LP_IO_CURRENT_STORAGE
local LP_IO_CURRENT_REQUIREMENT = logistics.meta_keys.LP_IO_CURRENT_REQUIREMENT
local LP_IO_MAX_INPUT = logistics.meta_keys.LP_IO_MAX_INPUT
local LP_IO_MAX_OUTPUT = logistics.meta_keys.LP_IO_MAX_OUTPUT

power_io.registered_power_blocks = {}

--- Register a node string as a valid power block.
function power_io.register_power_block(item_string, def)
	power_io.registered_power_blocks[item_string] = def
end

--- Check whether a block has previously been registered as a valid power block.
function power_io.is_power_io_block(pos)
	local node = minetest.get_node(pos)

	return power_io.registered_power_blocks[node.name] ~= nil
end

--- Get power IO object, or nil if not a power IO block.
function power_io.get(pos)
	if not power_io.is_power_io_block(pos) then
		log("warning", "Tried to get power IO for non power block.")
		return
	end

	return setmetatable(lp_io_node_cache:get(pos), power_io_meta)
end

--- Initialize power IO block. After this is called, all methods should be valid.
-- @todo: Doc
function power_io.create(pos, max_storage, input_faces, output_faces)
	local node_io_data = lp_io_node_cache:get(pos).serialized
	node_io_data[LP_IO_MASK] = AND(input_faces, RSHIFT(output_faces, 6))
	node_io_data[LP_IO_MAX_STORAGE] = max_storage
	node_io_data[LP_IO_CURRENT_STORAGE] = 0
	node_io_data[LP_IO_MAX_INPUT] = 100
	node_io_data[LP_IO_MAX_OUTPUT] = 100

	lp_io_node_cache:flush()

	return power_io.get(pos)
end

--   _____ _   _ _____  _    _ _______
--  |_   _| \ | |  __ \| |  | |__   __|
--    | | |  \| | |__) | |  | |  | |
--    | | | . ` |  ___/| |  | |  | |
--   _| |_| |\  | |    | |__| |  | |
--  |_____|_| \_|_|     \____/   |_|

--- Gets complete input mask
-- @todo LSHIFT is technically not required here.
-- @treturn number Mask for power IO
function power_io:get_input_mask()
	local io_mask = self.serialized[LP_IO_MASK]

	return AND(LSHIFT(io_mask, LP_IO_INPUT_OFFSET), ALL_FACES)
end

--- Sets complete input mask
-- @todo Assert mask is in range
-- @todo RSHIFT is technically not required here.
-- @tparam number mask New input mask for IO. Expected to be 0-63
function power_io:set_input_mask(mask)
	local io_mask = self.serialized[LP_IO_MASK]
	local bits_to_clear = RSHIFT(ALL_FACES, LP_IO_INPUT_OFFSET)
	io_mask = AND(io_mask, NOT(bits_to_clear))

	local bits_to_set = RSHIFT(mask, LP_IO_INPUT_OFFSET)
	io_mask = OR(io_mask, bits_to_set)

	self.serialized[LP_IO_MASK] = io_mask
	-- Important data - save instantly.
	lp_io_node_cache:flush()
end

--- Gets whether specific face is input
-- Technically can be used with any number of faces.
-- @todo Assert face is in range.
-- @tparam number face Face to get.
-- @treturn boolean Whether face is an input
function power_io:is_input_face(face)
	local io_mask = self.serialized[LP_IO_MASK]

	return AND(io_mask, RSHIFT(face, LP_IO_INPUT_OFFSET)) ~= 0
end

--- Enables a face input, and sets the associated list name.
-- @todo Assert face is valid.
-- @tparam number face Face to set.
function power_io:enable_face_input(face)
	local io_mask = self.serialized[LP_IO_MASK]
	local face_index = RSHIFT(face, LP_IO_INPUT_OFFSET)
	io_mask = OR(io_mask, face_index)

	self.serialized[LP_IO_MASK] = io_mask
	-- Important data - save instantly.
	lp_io_node_cache:flush()
end

--- Disables a face input, and clears the associated list name.
-- @todo Assert face is valid.
-- @tparam number face Face to disable.
function power_io:disable_face_input(face)
	local io_mask = self.serialized[LP_IO_MASK]
	local face_index = RSHIFT(face, LP_IO_INPUT_OFFSET)
	io_mask = AND(io_mask, NOT(face_index))

	self.serialized[LP_IO_MASK] = io_mask
	-- Important data - save instantly.
	lp_io_node_cache:flush()
end

--    ____  _    _ _______ _____  _    _ _______
--   / __ \| |  | |__   __|  __ \| |  | |__   __|
--  | |  | | |  | |  | |  | |__) | |  | |  | |
--  | |  | | |  | |  | |  |  ___/| |  | |  | |
--  | |__| | |__| |  | |  | |    | |__| |  | |
--   \____/ \____/   |_|  |_|     \____/   |_|

--- Gets complete output mask
-- @treturn number Mask for power IO
function power_io:get_output_mask()
	local io_mask = self.serialized[LP_IO_MASK]

	return AND(LSHIFT(io_mask, LP_IO_OUTPUT_OFFSET), ALL_FACES)
end

--- Sets complete output mask
-- @todo Assert mask is in range
-- @tparam number mask New output mask for IO. Expected to be 0-63
function power_io:set_output_mask(mask)
	local io_mask = self.serialized[LP_IO_MASK]
	local bits_to_clear = RSHIFT(ALL_FACES, LP_IO_OUTPUT_OFFSET)
	io_mask = AND(io_mask, NOT(bits_to_clear))

	local bits_to_set = RSHIFT(mask, LP_IO_OUTPUT_OFFSET)
	io_mask = OR(io_mask, bits_to_set)

	self.serialized[LP_IO_MASK] = io_mask
	-- Important data - save instantly.
	lp_io_node_cache:flush()
end

--- Gets whether specific face is output
-- Technically can be used with any number of faces.
-- @todo Assert face is in range.
-- @tparam number face Face to get.
-- @treturn boolean Whether face is an output
function power_io:is_output_face(face)
	local io_mask = self.serialized[LP_IO_MASK]

	return AND(io_mask, RSHIFT(face, LP_IO_OUTPUT_OFFSET)) ~= 0
end

--- Enables a face output, and sets the associated list name.
-- @todo Assert face is valid.
-- @tparam number face Face to set.
function power_io:enable_face_output(face)
	local io_mask = self.serialized[LP_IO_MASK]
	local face_index = RSHIFT(face, LP_IO_OUTPUT_OFFSET)
	io_mask = OR(io_mask, face_index)

	self.serialized[LP_IO_MASK] = io_mask
	-- Important data - save instantly.
	lp_io_node_cache:flush()
end

--- Disables a face output, and clears the associated list name.
-- @todo Assert face is valid.
-- @tparam number face Face to disable.
function power_io:disable_face_output(face)

	local io_mask = self.serialized[LP_IO_MASK]
	local face_index = RSHIFT(face, LP_IO_OUTPUT_OFFSET)
	io_mask = AND(io_mask, NOT(face_index))

	self.serialized[LP_IO_MASK] = io_mask
	-- Important data - save instantly.
	lp_io_node_cache:flush()
end

--    ____ _______ _    _ ______ _____
--   / __ \__   __| |  | |  ____|  __ \
--  | |  | | | |  | |__| | |__  | |__) |
--  | |  | | | |  |  __  |  __| |  _  /
--  | |__| | | |  | |  | | |____| | \ \
--   \____/  |_|  |_|  |_|______|_|  \_\

function power_io:get_max_power()
	return self.serialized[LP_IO_MAX_STORAGE]
end

-- @todo: Serialize?
function power_io:set_max_power(new_max_power)
	self.serialized[LP_IO_MAX_STORAGE] = new_max_power
end

function power_io:get_current_power()
	return self.serialized[LP_IO_CURRENT_STORAGE]
end

-- @todo: Bounds check
function power_io:set_current_power(new_power)
	local last_power = self.serialized[LP_IO_CURRENT_STORAGE]
	self.serialized[LP_IO_CURRENT_STORAGE] = new_power

	self:try_run_callback("on_power_change", self.pos, new_power - last_power, new_power)
end

function power_io:change_current_power(delta_power)
	self.serialized[LP_IO_CURRENT_STORAGE] = self.serialized[LP_IO_CURRENT_STORAGE] + delta_power

	self:try_run_callback("on_power_change", self.pos, delta_power, self.serialized[LP_IO_CURRENT_STORAGE])
end

function power_io:try_run_callback(callback_name, ...)
	local name = minetest.get_node(self.pos).name
	local power_node_def = power_io.registered_power_blocks[name]

	if not power_node_def then return end
	if type(power_node_def[callback_name]) == "function" then
		power_node_def[callback_name](...)
	end
end

function power_io:reset_power_requirement()
	self.transient[LP_IO_CURRENT_REQUIREMENT] = math.min(
		self.serialized[LP_IO_MAX_INPUT],
		self.serialized[LP_IO_MAX_STORAGE] - self.serialized[LP_IO_CURRENT_STORAGE]
	)
end
