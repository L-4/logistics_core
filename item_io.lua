--------------------
-- Item IO module
-- @module item_io

local item_io = {}

-- Make bitwise methods local
local RSHIFT = _G.RSHIFT
local LSHIFT = _G.LSHIFT
local AND = _G.AND
local OR = _G.OR
local NOT = _G.NOT

function logistics.get_item_io(pos)
    return setmetatable(item_io, {
        meta = minetest.get_meta(pos)
    })
end

local ALL_FACES = 63

local ITEM_IO_INPUT_OFFSET = 0
-- local ITEM_IO_OUTPUT_OFFSET = 6

local LOGISTICS_IO_MASK = "logistics_io_mask"

-- INPUT

--- Gets complete input mask
-- @treturn number Mask for IO
function item_io:get_input_mask()
    local io_mask = self.meta:get_int(LOGISTICS_IO_MASK)

    return AND(LSHIFT(io_mask, ITEM_IO_INPUT_OFFSET), ALL_FACES)
end

--- Sets complete input mask
-- @todo Assert mask is in range
-- @tparam number mask New input mask for IO. Expected to be 0-63
function item_io:set_input_mask(mask)
    local io_mask = self.meta:get_int(LOGISTICS_IO_MASK)
    local bits_to_clear = RSHIFT(ALL_FACES, ITEM_IO_INPUT_OFFSET)
    io_mask = AND(io_mask, NOT(bits_to_clear))

    local bits_to_set = RSHIFT(mask, ITEM_IO_INPUT_OFFSET)
    io_mask = OR(io_mask, bits_to_set)

    self.meta:set_int(LOGISTICS_IO_MASK, io_mask)
end

--- Gets whether specific face is input
-- Technically can be used with any number of faces.
-- @todo Assert face is in range.
-- @tparam number face Face to get.
-- @treturn boolean Whether face is an input
function item_io:is_input_face(face)
    local io_mask = self.meta:get_int(LOGISTICS_IO_MASK)

    return AND(io_mask, RSHIFT(face, ITEM_IO_INPUT_OFFSET)) ~= 0
end

--- Enables a face input, and sets the associated list name.
-- @todo Assert face is valid.
-- @tparam number face Face to set.
-- @tparam string list_name List name.
function item_io:enable_face_input(face, list_name)
    local io_mask = self.meta:get_int(LOGISTICS_IO_MASK)
    local face_index = RSHIFT(face, ITEM_IO_INPUT_OFFSET)
    io_mask = OR(io_mask, face_index)

    self.meta:set_string("logistics_inventory_" .. tostring(face_index), list_name)
    self.meta:set_int(LOGISTICS_IO_MASK, io_mask)
end

--- Disables a face input, and clears the associated list name.
-- @todo Assert face is valid.
-- @tparam number face Face to disable.
function item_io:disable_face_input(face)
    local io_mask = self.meta:get_int(LOGISTICS_IO_MASK)
    local face_index = RSHIFT(face, ITEM_IO_INPUT_OFFSET)
    io_mask = AND(io_mask, NOT(face_index))

    self.meta:set_string("logistics_inventory_" .. tostring(face_index), "")
    self.meta:set_int(LOGISTICS_IO_MASK, io_mask)
end

--- Returns input inv ref and list name for face.
-- @tparam number face Face to get inventory for
-- @treturn InvRef, string InvRef and list name for face.
function item_io:get_input_inventory_for_face(face)
    local face_index = RSHIFT(face, ITEM_IO_INPUT_OFFSET)

    local inventory = self.meta:get_inventory()
    local face_list_name = self.meta:get_string("logistics_inventory_" .. tostring(face_index))

    return inventory, face_list_name
end

--- Try to insert item stack, return remaining stack (might be empty).
-- @todo Assert face is input
-- @tparam number face Face to try to input from.
-- @tparam ItemStack stack Item stack to try to insert
-- @treturn ItemStack Remainder of stack.
function item_io:try_input_stack(face, stack)
    local inventory, list_name = self:get_input_inventory_for_face(face)

    return inventory:add_item(list_name, stack)
end

-- OUTPUT
-- Sets complete output mask
function item_io:get_output_mask() end
-- Gets complete output mask
function item_io:set_output_mask(mask) end
-- Gets whether specific face is output
function item_io:is_output_face(face) end
-- Sets whether specific face is output
function item_io:set_output_face(face, is_output) end
-- Returns output inv ref and list name for face.
function item_io:get_output_inventory_for_face(face) end
-- Try to take a stack from inventory. Might be empty.
function item_io:try_output_stack(face) end
-- Try to take n items up to limit. Returns array of stacks of items.
function item_io:try_output_items(face, limit) end
