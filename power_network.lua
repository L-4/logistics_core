--------------------
-- Power network module
-- @module power_network

local log = hutil.make_logger("Logistics Power Network")
local config_file = logistics.config_file.new("lpn_neworks", {
	networks = { },
})

local function assertx(assertion, fmt, ...)
	if not assertion then
		print(dump({ fmt, ... }))
		error(string.format(fmt, ...))
	else
		return assertion
	end
end

local function printo(val) print("Printo:") print(dump(val)) end

local world_path = minetest.get_worldpath() .. "/"
local function pronto(val)
	local f = io.open(world_path .. "pronto.lua", "w+")
	f:write(dump(val))
	f:close()
end

--- Helper function to get config file data.
local function lpn_data() return config_file.data end

local lpn_node_cache = logistics.node_cache.new("lpn")

local LP_NETWORK_ID = logistics.meta_keys.LP_NETWORK_ID

local power_network = {}
logistics.power_network = power_network

function power_network.clear_cache()
	config_file.data.networks = {}
	config_file:flush()

	lpn_node_cache:clear()

	log("info", "Cleared networks.")
end

minetest.register_chatcommand("lpn_clear", {
	description = "Deletes all power network IDs. Will probably break everything.",
	func = power_network.clear_cache,
})

minetest.register_chatcommand("lpn_list", {
	description = "Displays all power networks.",
	func = function()
		log("info", 'Power network string: "%s"', table.concat(config_file.data.networks, ","))
	end
})

-- BEGIN API

--- Creates a new power network.
-- @tparam Pos pos Position to start network floodfill at.
-- @treturn number New network ID.
function power_network.create_new(pos)
	local new_network_id = 1
	while lpn_data().networks[new_network_id] do new_network_id = new_network_id + 1 end

	local new_network = {
		-- pos = pos, @note: Try not to use this, it would make things very annoying to maintain!
		id = new_network_id,
		connected_machines = {},
		-- Current and max amount of power which can be stored in the network.
		current_network_storage = 0,
		max_network_storage = 0,
	}
	lpn_data().networks[new_network_id] = new_network
	config_file:flush()

	log("info", "Created network: #%d", new_network_id)

	return new_network
end

--- Deletes a power network.
-- @tparam number network_id Network to delete.
function power_network.delete(network_id)

	if not lpn_data().networks[network_id] then return end
	log("info", "Deleted network: #%d", network_id)

	lpn_data().networks[network_id] = nil

	config_file:flush()
end

-- @todo: Determine whether duplicating this functionality is worth it.

--- Gets an existing power network, and failes if it doesn't exist.
-- @tparam number network_id Network ID.
function power_network.get_by_id(network_id)
	assertx(network_id, "Tried to get network by id = falsy")
	assertx(network_id > 0, "Tried to get network from reserved network ID: #%d", network_id)
	return assertx(lpn_data().networks[network_id], "Tried to get invalid network #%d", network_id)
end

--- Gets an existing power network, or nil if it doesn't exist.
-- @tparam number network_id Network ID.
function power_network.try_get_by_id(network_id)
	if not network_id then return end
	return lpn_data().networks[network_id]
end

-- @todo: Document.
function power_network.get_by_pos(pos)
	assertx(pos, "Tried to get network with pos = nil")
	local network_id = assertx(lpn_node_cache:get(pos).serialized[LP_NETWORK_ID], "Failed to get network id at pos")
	return power_network.get_by_id(network_id)
end

-- @todo: Document.
function power_network.try_get_by_pos(pos)
	assertx(pos, "Tried to get network with pos = nil")
	local network_id = lpn_node_cache:get(pos).serialized[LP_NETWORK_ID]
	if not network_id then return end
	return power_network.get_by_id(network_id)
end

--- Aserts that this is a valid power_network block, and deletes block if not.
-- @treturn bool Returns true if node was valid.
function power_network.assert(pos)
	local cable_data = lpn_node_cache:get(pos).serialized
	local delet_this = false

	if not cable_data[LP_NETWORK_ID] then
		log("error", "Block does not contain 'LP_NETWORK_ID'.")
		delet_this = true
	elseif not power_network.get_by_id(cable_data[LP_NETWORK_ID]) then
		log("error", "No network associated with 'LP_NETWORK_ID'. (%d)",
			cable_data[LP_NETWORK_ID])
		delet_this = true
	end

	if delet_this then
		minetest.remove_node(pos)
		return false
	end

	print(string.format("Valid network: %d", cable_data[LP_NETWORK_ID]))

	return true
end

power_network.registered_power_cables = {}

--- Register a item string as a valid power cable.
-- @tparam string item_string Item string for block.
-- @tparam table def Power cable definition (currently not implemented)
-- @todo Make sure that def contains everything it has to.
-- def: {
-- 		max_io = 8000,
-- }
function power_network.register_power_cable(item_string, def)
	power_network.registered_power_cables[item_string] = def
end

-- @todo: For some of these, wouldn't it be better to store some generic transient
--		"being traversed" value?
local NETWORK_ID_GENERIC_INVALID = -1
local NETWORK_ID_IMPENDING_DELETION = -2

--- Sets network id for node at pos to ID, and does so recursively to all neighbors as well.
-- @tparam Pos pos Pos to floodfill from.
-- @tparam number id Network ID to set
-- @todo: Not super efficient to get meta at least twice per node.
-- @todo: Rename to floodfill_network_id or something, as we now have the need to floodfill other
--		properties.
local function floodfill_network(pos, id)
	local cable_data = lpn_node_cache:get(pos).serialized
	cable_data[LP_NETWORK_ID] = id

	-- Iterate through neighbors
	for neighbor_pos in hutil.iter_neighbors(pos) do
		local neighbor = minetest.get_node(neighbor_pos)
		-- @todo: Replace with is_cable or whatever.
		if power_network.registered_power_cables[neighbor.name] then
			local neighbor_network_id = lpn_node_cache:get(neighbor_pos).serialized[LP_NETWORK_ID]

			if neighbor_network_id ~= id
			and neighbor_network_id ~= NETWORK_ID_IMPENDING_DELETION then
				floodfill_network(neighbor_pos, id)
			end
		end
	end
end

-- @todo: Document
local network_traverse_dirty_count = 1

local function recalculate_network_power_capacity(pos, network_data, dirty_flag)
	-- Iterate through neighbors
	for neighbor_pos in hutil.iter_neighbors(pos) do
		local neighbor = minetest.get_node(neighbor_pos)
		-- @todo: Replace with is_cable or whatever.
		if power_network.registered_power_cables[neighbor.name] then
			local neighbor_cable_transient = lpn_node_cache:get(neighbor_pos).transient

			if neighbor_cable_transient.dirty_flag ~= dirty_flag then
				neighbor_cable_transient.dirty_flag = dirty_flag

				local neighbor_cable_data = power_network.registered_power_cables[neighbor.name]
				network_data.max_network_storage = network_data.max_network_storage + neighbor_cable_data.max_io

				recalculate_network_power_capacity(neighbor_pos, network_data, dirty_flag)
			end
		end
	end
end

--- Retraverse a network, and figure out it's current power capacity.
-- This function is the entry point, below is @todo: Finish writing this comment
local function begin_recalculate_network_power_capacity(origin_pos, network_id)
	local this_network_dirty_flag = network_traverse_dirty_count
	-- Increment dirty flag
	network_traverse_dirty_count = network_traverse_dirty_count + 1

	local network_data = power_network.get_by_id(network_id)
	-- Reset network storage.
	network_data.max_network_storage = 0

	recalculate_network_power_capacity(origin_pos, network_data, this_network_dirty_flag)
end

minetest.register_globalstep(function()
	pronto(lpn_node_cache)
end)

-- @todo: Go through again and sanity check the resolution of the new network max power amount.
function power_network.construct_cable(pos)
	-- There are three scenarios:
	-- 1. This cable is not connected to any power network: get new network ID.
	-- 2. This cable is connected to a network: take network ID from other network.
	-- 3. This cable connects two or more networks: choose one ID, and recursively
	--      eat the rest of the networks to make one.
	-- Also, might be worth building a list of power machines to append to whichever
	-- network wins the battle royale.

	-- Our data
	local cable_data = lpn_node_cache:get(pos)
	-- number network_id -> bool Only exists to weed out duplicate networks.
	local neighboring_networks = {}
	-- Array of all neighboring cables, regardless of network ID.
	local neighboring_cables = {}

	-- Iterate through neighbors
	for neighbor_pos in hutil.iter_neighbors(pos) do
		local neighbor = minetest.get_node(neighbor_pos)

		if power_network.registered_power_cables[neighbor.name] then
			local neighbor_cable_data = lpn_node_cache:get(neighbor_pos)
			local neighbor_network_id = neighbor_cable_data.serialized[LP_NETWORK_ID]

			-- Store in sparse array (interally hashmap) as to weed duplicate networks early.
			neighboring_networks[neighbor_network_id] = true
			neighboring_cables[#neighboring_cables + 1] = {
				pos = neighbor_pos,
				network_id = neighbor_network_id,
			}
		end
	end

	-- Number of neighboring networks. Cached as we store neighboring networks in map, not array.
	local neighboring_network_count = 0
	-- This should contain the ID of an arbitrary neighboring network, if one
	-- exists. This is the network which will stay if another network needs to be removed.
	local neighboring_network_id = NETWORK_ID_GENERIC_INVALID
	-- We might have to delete networks, in which case we'd like to eat their power.
	local deleted_network_power_capacity = 0
	-- Not fantastically efficient, but this has a max size of six elements.
	for network_id in pairs(neighboring_networks) do
		neighboring_network_count = neighboring_network_count + 1
		neighboring_network_id = network_id
	end

	-- Forward declared network ID which should be assigned to when we know what the ID of
	-- the current node should be.
	local resulting_network_id

	if neighboring_network_count == 0 then
		-- Nothing to connect to - make new network and move on.
		local new_network = power_network.create_new(pos)

		resulting_network_id = new_network.id
	elseif neighboring_network_count == 1 then
		-- There's only one network connected, so let's just eat the network id and move on.
		-- We know for a fact that this must have been stored in neighboring_network_id since
		-- there was only one neighbor.

		resulting_network_id = neighboring_network_id
	else
		-- Delete all neighboring networks which aren't the one we've chosen to win.
		-- Here we store a map of networks which have been deleted, as to not delete them twice.
		local deleted_networks = {}
		for _, neighbor in ipairs(neighboring_cables) do
			if neighbor.network_id ~= neighboring_network_id and
				not deleted_networks[neighbor.network_id] then
				local neighbor_network = power_network.get_by_id(neighbor.network_id)
				-- I ate those power
				deleted_network_power_capacity = deleted_network_power_capacity + neighbor_network.max_network_storage
				power_network.delete(neighbor.network_id)
				deleted_networks[neighbor.network_id] = true
			end
		end

		-- We've just connected two or more networks. They all need to become one.
		-- We've already picked an arbitrary network neighboring_network_id earier, so let'
		-- just floodfill the current tile with said ID.
		floodfill_network(pos, neighboring_network_id)

		resulting_network_id = neighboring_network_id
	end

	assert(resulting_network_id, "No network ID decided upon!")
	cable_data.serialized[LP_NETWORK_ID] = resulting_network_id

	local network = power_network.get_by_id(resulting_network_id)
	local cable_name = minetest.get_node(pos).name
	local cable_def = power_network.registered_power_cables[cable_name]
	-- @todo: Coalesce (I had to look up the spelling) the power stored in deleted networks.
	network.max_network_storage = network.max_network_storage + cable_def.max_io + deleted_network_power_capacity

	-- -- @todo: We iterate in the exact same way earlier. Join the loops?
	-- for neighbor_pos in hutil.iter_neighbors(pos) do
	-- 	if logistics.power_io.is_power_io_block(neighbor_pos) then
	-- 		network.connected_machines[minetest.hash_node_position(neighbor_pos)] = true
	-- 	end
	-- end

	config_file:flush()
end

-- @todo: Go through again and sanity check the resolution of the new network max power amount.
--		(Case three is not yet implemented)
function power_network.destruct_cable(pos)
	-- Again like in the ctor, there are the same three scenarios:
	-- 1. This cable is not connected to a network: delete network.
	-- 2. This is a terminal node in a network: do nothing at all.
	-- 3. This cable neighbors two or more cables. This one is by far the most difficult,
	--      as it itself has three different scenarios (keep in mind that all neighboring
	--      networks should, if everything is correct, have the same ID):
	--      3.1: The neighboring cables are truly part of the same network, Ie they connect
	--          somewhere else: do nothing.
	--      3.2: The neighboring cables do not connect to each other down the line: pick one
	--          network to keep, and floodfill the others with a new ID.
	--      3.3 (kinda): A combination of the above two. This is different from when constructing
	--          a cable, as in that case we can assume that we will only end up with one network.

	-- So here's my solution. There are many like it, but this one is mine.
	-- In case three, floodfill each neighboring cable with a new network. The first neighbor can
	--      be excluded from this, since you can consider it to be "pre floodfilled" with it's own
	--      network.
	-- Keep track of each new network ID used to floodfill. Once this is complete, iterate through
	--      all of the neighboring networks again, and delete any networks which do not exist in
	--      the network anymore. These networks can only have been part of a hidden loop between
	--      two other neighbors.
	-- This isn't a super efficient solution, as it requires floodfilling several arbirarily
	--      large networks in rapid succession.

	-- Well, here goes.

	-- Our data
	local cable_data = lpn_node_cache:get(pos).serialized
	-- Our network ID
	local this_network_id = cable_data[LP_NETWORK_ID]
	assertx(type(this_network_id) == "number", "this_network_id was not a number")
	assertx(this_network_id > 0, "this_network_id was <= 0 (was %d)", this_network_id)
	-- This network
	local this_network = power_network.get_by_id(this_network_id)
	-- Array of all neighboring cables. They all have the same network ID, so only store pos.
	local neighboring_cables = {}

	-- Iterate through neighbors
	for neighbor_pos in hutil.iter_neighbors(pos) do
		local neighbor = minetest.get_node(neighbor_pos)

		-- @todo: Replace with is_cable or whatever.
		if power_network.registered_power_cables[neighbor.name] then
			neighboring_cables[#neighboring_cables + 1] = {
				pos = neighbor_pos,
			}
		end
	end

	if #neighboring_cables == 0 then
		power_network.delete(this_network_id)
	elseif #neighboring_cables == 1 then
		-- Only one neighbor, so let's just subtract this cable's power from that network.
		-- @todo: A lot of asserts are needed here.
		local neighboring_cable_data = assert(lpn_node_cache:get(neighboring_cables[1].pos).serialized)
		local this_cable_name = assert(minetest.get_node(pos).name)
		local this_cable_max_io = assert(power_network.registered_power_cables[this_cable_name]).max_io
		assert(neighboring_cable_data[LP_NETWORK_ID])
		local neighboring_network_data = assert(lpn_data().networks[neighboring_cable_data[LP_NETWORK_ID]])

		neighboring_network_data.max_network_storage =
			neighboring_network_data.max_network_storage - this_cable_max_io

	elseif #neighboring_cables > 1 then
		-- We're in the complicated branch.

		-- List of floodfilled networks. To make this process slightly faster,
		-- we assume that the first neighbor is floodfilled with the same network ID
		-- as this cable, which should be the case.
		local floodfilled_networks = {
			[this_network.id] = { pos = pos, deleted = true }
		}

		-- Here we tell the floodfill that this is not a valid cable anymore.
		cable_data[LP_NETWORK_ID] = NETWORK_ID_IMPENDING_DELETION

		log("info", "This network ID: %d", this_network.id)

		-- Start with the second neighbor, since we dealt with the first one above.
		-- Here we floodfill each neighbor with a new unique network ID.
		for neighbor_idx = 2, #neighboring_cables do
			local neighbor = neighboring_cables[neighbor_idx]

			assertx(neighbor.pos, "No pos")
			local new_network = power_network.create_new(neighbor.pos)
			log("info", "Created temp network: %d", new_network.id)
			assertx(new_network.id ~= this_network.id, "Duplicate networks: this = %d, new = %d",
				this_network.id, new_network.id)
			floodfilled_networks[new_network.id] = { pos = neighbor.pos, deleted = true }

			floodfill_network(neighbor.pos, new_network.id)
		end

		for neighbor_idx = 1, #neighboring_cables do
			local neighbor = neighboring_cables[neighbor_idx]
			local neighbor_cable_data = lpn_node_cache:get(neighbor.pos).serialized
			local neighbor_network_id = neighbor_cable_data[LP_NETWORK_ID]
			log("info", "Network %d confirmed to still exist", neighbor_network_id)

			floodfilled_networks[neighbor_network_id].deleted = false
		end

		for network_id, network_info in pairs(floodfilled_networks) do
			if network_info.deleted then
				power_network.delete(network_id)
			else
				begin_recalculate_network_power_capacity(network_info.pos, network_id)
			end
		end

	end

	lpn_node_cache:delete(pos)
end

--- Update:
-- 1. Update IO budget for all power blocks, and build list of input/output machines
-- 2.

---
-- 1. Reset all network's power budget
-- 2. iterate through outputting machines, add to neighboring budgets as possible

-- 1. Gather power requirements per network.
-- For each machine, figure out it's power requirement.
-- If there's only one connected
-- function power_network.update(delta)
-- 	-- Reset the power requirements of all networks.
-- 	for _, network in pairs(lpn_data().networks) do
-- 		-- @todo: This will be serialized for no reason. Fix this by adding transient table to
-- 		-- the config file.
-- 		network.required_power = 0
-- 	end

-- 	for node_data in logistics.power_io.node_cache:iter() do
-- 		local machine_power_io = logistics.power_io.get(node_data.pos)
-- 		machine_power_io:reset_power_requirement()
-- 	end
-- end


--- Options
-- 1. Have IO buffer in cables, treat it as an energy storage device
-- By definition, will resolve the power
-- Has one tick delay on IO
-- You have to figure out the total capacity of the network, or set a good one
--		manually.
