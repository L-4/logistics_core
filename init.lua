local logistics_path = minetest.get_modpath("logistics_core")

--- Utility library
dofile(logistics_path.."/hutil.lua")

local log = hutil.make_logger("Logistics Core")

log("info", "Hello from Logistics core!")

--- General @todo:
-- Would there be a way to store all IO masks in one int? Currently it's looking like
--  around 36 bits...

logistics = {}

logistics.config_file = dofile(logistics_path.."/config_file.lua")
logistics.node_cache = dofile(logistics_path.."/node_cache.lua")

--- Block facing bitmask
logistics.PLUS_X  = 1
logistics.MINUS_X = 2
logistics.PLUS_Y  = 4
logistics.MINUS_Y = 8
logistics.PLUS_Z  = 16
logistics.MINUS_Z = 32

--- All of the meta keys used in logistics. This can be used to either shorten them if that has an
-- impact on network performance, or just to make sure that nothing is ever mispelled.
-- @todo Convert all instances of meta keys with this.
logistics.meta_keys = {
    -- Logistics Power IO
    LP_IO_MASK                = "lp_io_mask",                -- IO enabled mask (12 bits)
    LP_IO_MAX_STORAGE         = "lp_io_max_storage",         -- Max storage in node
    LP_IO_CURRENT_STORAGE     = "lp_io_current_storage",     -- Current storage in node
    LP_IO_CURRENT_REQUIREMENT = "lp_io_current_requirement",
    LP_IO_MAX_INPUT           = "lp_io_max_input",
    LP_IO_MAX_OUTPUT          = "lp_io_max_output",
    -- Logistics Power Network
    LP_NETWORK_ID = "lp_network_id"
}

dofile(logistics_path.."/power_network.lua")
dofile(logistics_path.."/power_io.lua")

-- Four modules: power, (items, fluids, network)
-- Each module contains:
--      module_io:
-- Module io is a meta type which deals with all block-specific, network unaware
-- code. This means stuff like: which faces are inputs/outputs? How much storage
-- space is in this block? What is the i/o limit per tick? (maybe functions to
-- input/output, but more likely this will be an intrinsic thing, managed by
-- properties only).
--      module_network:
-- Module network is a namespace with a) helper functions for the module_io
-- module to work, and b) manages the networks themselves, pulling and pushing
-- data as needed. This is the driving force for the actual behavior of the
-- networks.

-- Things which don't fit nicely anywhere:

-- Where do methods which register new <module> io/network nodes go?
--      As an example, it doesn't make complete sense to me that registering
--      a new power IO node should come from power_io.register(), since power_io
--      already represents the metatable of the blocks themselves.

-- Should the <module>_networks be a meta type as well, or should they just be a
--      namespace?

-- What data should be serialized?

-- Does it make sense to have the public-facing API for block faces just use
--      simple tables with relative block faces, and then use bitmasks internally?
