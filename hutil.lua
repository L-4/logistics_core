hutil = {}

local offsets = {
    { x = 1, y = 0, z = 0 },
    { x = 0, y = 1, z = 0 },
    { x = 0, y = 0, z = 1 },
    { x = -1, y = 0, z = 0 },
    { x = 0, y = -1, z = 0 },
    { x = 0, y = 0, z = -1 },
}

function hutil.iter_neighbors(base_pos)
    local i = 1

    return function()
        if i > 6 then return end

        local offset = offsets[i]
        i = i + 1

        return {
            x = base_pos.x + offset.x,
            y = base_pos.y + offset.y,
            z = base_pos.z + offset.z,
        }
    end
end

function hutil.make_logger(prefix)
    return function(log_type, fmt, ...)
        print(
            string.format("[%s] [%s] ", prefix, log_type) ..
            string.format(fmt, ...)
        )
    end
end
