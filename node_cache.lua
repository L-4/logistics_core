--------------------
-- Node cache module. Exists to circumvent MetaRef.
-- @module node_cache
-- @todo: All of this data has to be stored in memory at once, not good.
-- @todo: Once this feels final-ish, add docs.

local node_cache = {}
node_cache.__index = node_cache

local world_path = minetest.get_worldpath() .. "/"

local hashma = minetest.hash_node_position
local unhashma = minetest.get_position_from_hash

local function file_exists(path)
	local f = io.open(path, "r")
	local exists = f ~= nil
	if exists then f:close() end
	return exists
end

--- Create new config file.
-- @tparam string name Name of node cache
function node_cache.new(name)
	-- Qualified path
	local path = world_path .. name .. ".lua"
	-- Directory to file.
	local file_directory = path:match("(.+)/")
	minetest.mkdir(file_directory)

	local instance = setmetatable({
		path = path,
		data_array = {}, -- Data is array of { serialized, transient, pos, hash },
		serialized_root = {}, -- Why do all of these names suck.
		data_by_hash = {},
	}, node_cache)

	if not file_exists(path) then
		-- Save empty data.
		instance:flush()
	else
		instance:read()
	end

	minetest.register_on_shutdown(function() instance:flush() end)

	return instance
end

--- Flush the contents of self.serialized_root to the config file.
function node_cache:flush()
	local data = minetest.serialize(self.serialized_root)
	local file = io.open(self.path, "w+")
	file:write(data)
	file:close()
end

--- Reads the contents of the config file to self.data
-- @todo: Rename to "load", or something?
function node_cache:read()
	local file = assert(io.open(self.path, "r"))
	local file_string = file:read("*a")
	file:close()

	local file_data = minetest.deserialize(file_string)

	self.data_array = {}

	local node_idx = 1
	for node_hash, node_data in pairs(file_data) do
		local node_pos = unhashma(node_hash)
		self.data_array[node_idx] = {
			transient = {},
			serialized = node_data,
			pos = node_pos,
			hash = node_hash,
		}

		self.serialized_root[node_hash] = node_data
		self.data_by_hash[node_hash] = self.data_array[node_idx]

		node_idx = node_idx + 1
	end
end

function node_cache:get(pos)
	local hash = hashma(pos)

	if not self.data_by_hash[hash] then
		local new_idx = #self.data_array + 1
		self.data_array[new_idx] = {
			transient = {},
			serialized = {},
			pos = pos,
			hash = hash,
		}

		self.serialized_root[hash] = self.data_array[new_idx].serialized
		self.data_by_hash[hash] = self.data_array[new_idx]

		self:flush()
	end

	return self.data_by_hash[hash]
end

function node_cache:delete(pos)
	local hash = hashma(pos)

	self.serialized_root[hash] = nil
	self.data_by_hash[hash] = nil

	for data_idx, node_data in ipairs(self.data_array) do
		if node_data.hash == hash then
			self.data_array[data_idx] = nil
			break
		end
	end

	self:flush()
end

function node_cache:iter()
	local idx = 0
	local data_array = self.data_array
	return function()
		idx = idx + 1
		return data_array[idx]
	end
end

--- Clear all cached data, and save.
function node_cache:clear()
	self.data_array = {}
	self.serialized_root = {}
	self.data_by_hash = {}

	self:flush()
end

return node_cache
