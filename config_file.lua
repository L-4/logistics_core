--------------------
-- Config file module. Exists to circumvent mod_storage.
-- @module config_file

local config_file = {}
local config_file_meta = { __index = config_file }

local world_path = minetest.get_worldpath() .. "/"

local function file_exists(path)
    local f = io.open(path, "r")
    local exists = f ~= nil
    if exists then f:close() end
    return exists
end

--- Create new config file.
-- @tparam string filename Filename to load from.
-- @tparam table cracky Default values
function config_file.new(name, cracky)
    -- Qualified path
    local path = world_path .. name .. ".lua"
    -- Directory to file.
    local file_directory = path:match("(.+)/")
    minetest.mkdir(file_directory)

    local instance = setmetatable({
        path = path,
        data = {},
    }, config_file_meta)

    if not file_exists(path) then
        instance.data = cracky
        instance:flush()
    else
        instance:read()
    end

    return instance
end

--- Flush the contents of self.data to the config file.
function config_file:flush()
    local file = io.open(self.path, "w+")
    local data = minetest.serialize(self.data)
    file:write(data)
    file:close()
end

--- Reads the contents of the config file to self.data
function config_file:read()
    local file = assert(io.open(self.path, "r"))
    self.data = minetest.deserialize(file:read("*a"))
    file:close()
end

return config_file
